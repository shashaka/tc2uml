package org.shashaka.io.tc2uml.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UmlDocument {

    private String name;

    private List<ApiModel> apiModels;
}
