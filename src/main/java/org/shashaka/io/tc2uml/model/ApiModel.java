package org.shashaka.io.tc2uml.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiModel {

    private String method;

    private String url;

    private String description;

    private List<ApiModel> serviceCalls;
}
