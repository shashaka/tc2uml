package org.shashaka.io.tc2uml.config;

import lombok.RequiredArgsConstructor;
import org.mockito.internal.invocation.InterceptedInvocation;
import org.mockito.listeners.InvocationListener;
import org.mockito.listeners.MethodInvocationReport;
import org.shashaka.io.tc2uml.model.ApiModel;
import org.shashaka.io.tc2uml.utils.UmlGenerator;
import org.springframework.http.HttpMethod;

import java.net.URI;
import java.net.URL;

@RequiredArgsConstructor
public class UmlInvocationListener implements InvocationListener {

    private final UmlGenerator umlGenerator;

    @Override
    public void reportInvocation(MethodInvocationReport methodInvocationReport) {
        ApiModel apiModel = new ApiModel();

        HttpMethod httpMethod;
        String url = null;
        if (methodInvocationReport != null) {
            InterceptedInvocation invocation = (InterceptedInvocation) methodInvocationReport.getInvocation();
            if (invocation != null && invocation.getArguments() != null) {
                httpMethod = getHttpMethod(invocation.getMethod().getName(), invocation.getArguments());
                for (Object argument : invocation.getArguments()) {
                    if (argument instanceof String) {
                        String str = (String) argument;
                        if (str.contains("http")) {
                            url = str;
                        }
                    } else if (argument instanceof URI) {
                        url = argument.toString();
                    } else if (argument instanceof URL) {
                        url = argument.toString();
                    }
                }
                if (httpMethod != null && url != null) {
                    apiModel.setMethod(httpMethod.name());
                    apiModel.setUrl(url);
                    umlGenerator.addServiceCall(apiModel);
                }
            }
        }
    }

    private HttpMethod getHttpMethod(String name, Object[] arguments) {
        if (name.toLowerCase().contains("post")) {
            return HttpMethod.POST;
        } else if (name.toLowerCase().contains("put")) {
            return HttpMethod.PUT;
        } else if (name.toLowerCase().contains("get")) {
            return HttpMethod.GET;
        } else if (name.toLowerCase().contains("delete")) {
            return HttpMethod.DELETE;
        } else if (name.toLowerCase().contains("exchange")) {
            for (Object argument : arguments) {
                if (argument instanceof HttpMethod) {
                    return (HttpMethod) argument;
                }
            }
        }
        return null;
    }
}
