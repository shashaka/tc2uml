package org.shashaka.io.tc2uml.utils;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.shashaka.io.tc2uml.model.ApiModel;
import org.shashaka.io.tc2uml.model.UmlDocument;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class UmlGenerator {

    private static UmlDocument umlDocument = new UmlDocument();

    private static ApiModel api;

    @PostConstruct
    public void init() {
        umlDocument.setName("API_server");
    }

    public void addApi(ApiModel apiModel) {
        List<ApiModel> apiModels = umlDocument.getApiModels();
        if (apiModels == null) {
            apiModels = new ArrayList<>();
            umlDocument.setApiModels(apiModels);
        }
        apiModels.add(apiModel);
        api = apiModel;
    }

    public void addServiceCall(ApiModel apiModel) {
        List<ApiModel> serviceCalls = api.getServiceCalls();
        if (serviceCalls == null) {
            serviceCalls = new ArrayList<>();
            api.setServiceCalls(serviceCalls);
        }
        serviceCalls.add(apiModel);
    }

    @SneakyThrows
    public UmlDocument writeUmlDocument() {

        StringBuilder builder = new StringBuilder();

        for (ApiModel apiModel : umlDocument.getApiModels()) {
            String fileName = apiModel.getMethod() + "_" + apiModel.getUrl().replace("/", "_");
            builder.append("@startuml\n");
            builder.append("title ").append(fileName).append("\\n\n");

            builder.append("User -> ")
                    .append(umlDocument.getName())
                    .append(" : ")
                    .append(apiModel.getMethod())
                    .append(" ")
                    .append(apiModel.getUrl())
                    .append("\n");

            for (ApiModel serviceCall : apiModel.getServiceCalls()) {
                builder.append(umlDocument.getName())
                        .append(" -> ")
                        .append(URI.create(serviceCall.getUrl()).getHost())
                        .append(" : ")
                        .append(serviceCall.getMethod())
                        .append(" ")
                        .append(serviceCall.getUrl())
                        .append("\n");
            }

            builder.append("@enduml");
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
                writer.write(builder.toString());
                builder.setLength(0);
            } catch (Exception e) {
                log.error("exception :", e);
            }
        }
        return umlDocument;
    }
}
