package org.shashaka.io.tc2uml;

import org.mockito.Mockito;
import org.mockito.internal.creation.MockSettingsImpl;
import org.mockito.invocation.InvocationOnMock;
import org.shashaka.io.tc2uml.config.UmlInvocationListener;
import org.shashaka.io.tc2uml.filter.UmlDocFilter;
import org.shashaka.io.tc2uml.utils.UmlGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.web.client.RestTemplate;

@Configuration
@ComponentScan("org.shashaka.io.tc2uml")
public class Tc2UmlConfig {

    @Autowired
    private UmlGenerator umlGenerator;

    @Bean
    public MockMvc mockMvc(DefaultMockMvcBuilder builder) {
        return builder
                .addFilters(new UmlDocFilter(umlGenerator))
                .build();
    }

    @Primary
    @Bean
    public RestTemplate restTemplate() {
        return Mockito.mock(RestTemplate.class,
                new MockSettingsImpl<>()
                        .defaultAnswer(InvocationOnMock::getMock)
                        .invocationListeners(new UmlInvocationListener(umlGenerator)));
    }
}
