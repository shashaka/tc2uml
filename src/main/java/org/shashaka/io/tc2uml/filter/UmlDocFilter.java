package org.shashaka.io.tc2uml.filter;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.shashaka.io.tc2uml.model.ApiModel;
import org.shashaka.io.tc2uml.utils.UmlGenerator;
import org.springframework.mock.web.MockHttpServletRequest;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

@Slf4j
@AllArgsConstructor
public class UmlDocFilter implements Filter {

    private final UmlGenerator umlGenerator;

    @SneakyThrows
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) {

        MockHttpServletRequest mockHttpServletRequest = (MockHttpServletRequest) request;

        ApiModel apiModel = new ApiModel();
        apiModel.setMethod(mockHttpServletRequest.getMethod());
        apiModel.setUrl(mockHttpServletRequest.getRequestURI());
        umlGenerator.addApi(apiModel);

        chain.doFilter(request, response);
    }
}
